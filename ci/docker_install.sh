#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install libonig-dev libpq-dev libcurl4-openssl-dev libicu-dev libmcrypt-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev libzip-dev git -yqq

# Install phpunit, the tool that we will use for testing
# curl --location --output /usr/local/bin/phpunit "https://phar.phpunit.de/phpunit.phar"
# chmod +x /usr/local/bin/phpunit

# Compile PHP, include these extensions.
docker-php-ext-install mbstring pdo curl json intl gd xml zip bz2 opcache