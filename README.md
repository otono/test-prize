# Тестовое задание
### Сборка

```sh
composer install
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load
php bin/console cache:warmup
```

### Команда для отправки денег на счёт в банке
`php bin/console app:send_money`

### Тесты
`php bin/phpunit`