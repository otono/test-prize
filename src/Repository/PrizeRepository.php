<?php

namespace App\Repository;

use App\Entity\Prize;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Prize|null find($id, $lockMode = null, $lockVersion = null)
 * @method Prize|null findOneBy(array $criteria, array $orderBy = null)
 * @method Prize[]    findAll()
 * @method Prize[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrizeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Prize::class);
    }

    /**
     * @return int|mixed|string
     */
    public function getPendingBankRequestsQuery()
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.type = :type')
            ->andWhere('p.processStatus = :processStatus')
            ->setParameter('type', 'money')
            ->setParameter('processStatus', 0)
            ->getQuery()
        ;
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPendingBankReuestsCount()
    {
        return $this->createQueryBuilder('p')
            ->select('COUNT(p.id) as pendingCount')
            ->andWhere('p.type = :type')
            ->andWhere('p.processStatus = :processStatus')
            ->setParameter('type', 'money')
            ->setParameter('processStatus', 0)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
