<?php

namespace App\DataFixtures;

use App\Entity\Item;
use App\Entity\Param;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class AppFixtures extends Fixture
{
    private ContainerBagInterface $params;

    public function __construct(ContainerBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        // Set app balance
        $param = new Param();
        $param->setKey('balance');
        $param->setValue($this->params->get('app.balance'));
        $manager->persist($param);

        // Convert coefficient
        $param = new Param();
        $param->setKey('coefficient');
        $param->setValue($this->params->get('app.coefficient'));
        $manager->persist($param);

        // Set items
        $items = ['перестанавливание','респектабельность','бензилфениламин','декагидронафтол','видоначертание','газообильность','динитробензоил','палеоакватория','подверженность','посягательство','пропилгидразин','влагопередача','компрессорщик','контрвнушение','макроовалоцит','метилкарнозин','спецперевозка','стриопаллидум','терманестезия','шёлкокручение','белоцерковец','вифлеемлянин','встрачивание','психотехника','скважистость','техреволюция','химиовакцина','хориоаденома','анемография','бензпиридин','обмётывание','породосмена','процветание','стеркобилин','тератология','доксиламин','жилистость','макросдвиг','пиролатрия','предикатив','скотландит','тазовчанин','темниковец','фотосинтез','аффриката','берсальер','габелинец','круглопер','майоритет','менизидин','периодика','плеоптика','политевма','провалище','синдесмоз','статотдел','тиоуразол','чечевичка','анагирин','вохмянка','гамартия','десмозит','калиптра','пеипунин','пестовец','почивший','проходка','свиднеит','статский','стиховен','струение','тригония','фотограф','чесночок','шмыгание','бетафит','вакуоль','иудофил','кельнец','лабамки','немоляк','подпись','реборда','ружанин','эсперит','акабар','звучок','пекмес','прядка','фразер','хостел','цианея','вакия','вятец','литол','пигва','плита','фузит','боёк','цеж'];

        foreach ($items as $name) {
            $item = new Item();
            $item->setName($name);
            $item->setQuantity(1);
            $manager->persist($item);
        }

        $manager->flush();
    }
}
