<?php

declare(strict_types=1);

namespace App\Bank;

use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Client
{
    private string $baseUrl;
    private string $token;
    private HttpClientInterface $client;

    /**
     * Client constructor.
     * @param HttpClientInterface $client
     * @param ContainerBagInterface $params
     */
    public function __construct(HttpClientInterface $client, ContainerBagInterface $params)
    {
        $this->client = $client;
        $this->baseUrl = $params->get('bank.api');
        $this->token = $params->get('bank.token');
    }

    /**
     * @param string $type
     * @param string $url
     * @param array $body
     * @return \Symfony\Contracts\HttpClient\ResponseInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function request(string $type, string $url, array $body = [])
    {
        return $this->client->request($type, sprintf('%s%s', $this->baseUrl, $url), [
            'headers' => [
                'Content-Type' => 'application/json',
                'X-Auth-Token' => getenv('BANK_TOKEN')
            ],
            'json' => $body
        ]);
    }

    /**
     * Отправка денег, предположим, что $email это идентификатор, для упрощения
     * @param string $email
     * @param float $amount
     * @return \Symfony\Contracts\HttpClient\ResponseInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function sendMoney(string $email, float $amount)
    {
        return $this->request('POST', 'send_money', [
            'email' => $email,
            'amount' => $amount
        ]);
    }
}