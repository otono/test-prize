<?php

declare(strict_types=1);

namespace App\Lottery\Prize;

use App\Lottery\Prize;

class Item extends Prize implements ItemInterface
{
    /**
     * @throws \Exception
     */
    public function generateRandomPrize(): void
    {
        $this->item = $this->params['prize.items'][random_int(0, count($this->params['prize.items']))];
    }

    /**
     * @return bool
     */
    public function checkAvailability(): bool
    {
        return count($this->params['prize.items']) > 0;
    }
}