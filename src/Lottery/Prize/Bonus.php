<?php

declare(strict_types=1);

namespace App\Lottery\Prize;

use App\Lottery\Prize;

class Bonus extends Prize implements BonusInterface
{
    /**
     * @throws \Exception
     */
    public function generateRandomPrize(): void
    {
        $this->amount = random_int($this->params['prize.bonus.min'], $this->params['prize.bonus.max']);
    }

    /**
     * @return bool
     */
    public function checkAvailability(): bool
    {
        return true;
    }
}