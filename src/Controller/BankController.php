<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BankController extends AbstractController
{
    /**
     * @return Response
     */
    public function index(): Response
    {
        return $this->json([
            'status' => 'ok'
        ]);
    }
}
