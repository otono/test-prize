<?php

declare(strict_types=1);

namespace App\EntityListener;

use App\Entity\Prize;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

class PrizeEntityListener
{
    private EntityManagerInterface $entityManager;

    /**
     * PrizeEntityListener constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Prize $prize
     * @param LifecycleEventArgs $event
     */
    public function postPersist(Prize $prize, LifecycleEventArgs $event)
    {
        $item = $prize->getItem();

        // Reduce Item quantity
        if ($item) {
            $item->setQuantity($item->getQuantity() - 1);
            $this->entityManager->persist($item);
            $this->entityManager->flush();
        }
    }
}