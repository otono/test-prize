<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Prize;
use App\Lottery\BonusConverter;
use App\Lottery\Prize\Bonus;
use App\Lottery\Prize\Item;
use App\Lottery\Prize\Money;
use App\Repository\ItemRepository;
use App\Repository\ParamRepository;
use App\Repository\PrizeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Security\Core\Security;

class PrizeService
{
    private array $params = [];
    private EntityManagerInterface $em;
    private ParamRepository $paramRepository;
    private ItemRepository $itemRepository;
    private PrizeRepository $prizeRepository;
    private Security $security;
    private BonusConverter $bonusConverter;

    const PRIZES = [
        Bonus::class,
        Money::class,
        Item::class,
    ];

    /**
     * PrizeService constructor.
     * @param ContainerBagInterface $params
     * @param EntityManagerInterface $em
     * @param ParamRepository $paramRepository
     * @param ItemRepository $itemRepository
     * @param PrizeRepository $prizeRepository
     * @param Security $security
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __construct(
        ContainerBagInterface $params,
        EntityManagerInterface $em,
        ParamRepository $paramRepository,
        ItemRepository $itemRepository,
        PrizeRepository $prizeRepository,
        Security $security,
        BonusConverter $bonusConverter
    ) {
        $this->em = $em;
        $this->paramRepository = $paramRepository;
        $this->itemRepository = $itemRepository;
        $this->prizeRepository = $prizeRepository;
        $this->security = $security;
        $this->bonusConverter = $bonusConverter;

        // Set params
        $this->setParams($params->all());
    }

    public function makePrize(): void
    {
        $lotteryPrize = \App\Lottery\Prize::createPrize(self::PRIZES, $this->params);
        $lotteryPrize->generateRandomPrize();

        // Save prize
        $prize = new \App\Entity\Prize();
        $prize->setType($lotteryPrize->getType());
        $prize->setAmount($lotteryPrize->getAmount());

        // Item prize
        if ($lotteryPrize instanceof Item) {
            $item = $this->itemRepository->find($lotteryPrize->getItem()['id']);
            $prize->setItem($item);
        }

        $prize->setUser($this->security->getUser());

        $this->em->persist($prize);
        $this->em->flush();
    }

    /**
     * Set app params
     * @param array $params
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function setParams(array $params)
    {
        // Set default params
        foreach ($params as $key => $value) {
            if (stripos($key, 'prize.') !== false) {
                $this->params[$key] = $value;
            }
        }

        // Money limit
        $balance = $this->paramRepository->getBalanceParam();
        $this->params['prize.balance'] = (int) $balance->getValue();

        // Items count
        $this->params['prize.items'] = $this->itemRepository->getActiveItems();

        // Convert coefficient
        $coefficient = $this->paramRepository->getCoefficientParam();
        $this->params['prize.coefficient'] = (float) $coefficient->getValue();
    }

    /**
     * @param int $id
     * @param string $action
     * @param string|null $address
     * @return bool
     */
    public function processPrize(int $id, string $action, ?string $address): bool
    {
        $prize = $this->prizeRepository->find($id);

        if (!$prize instanceof Prize) {
            return false;
        }

        $actionMethod = 'process'.ucfirst($action).'Action';
        if (!method_exists(self::class, $actionMethod)) {
            return false;
        }

        $this->$actionMethod($prize, $address);

        // Update prize
        $prize->setStatus(Prize::STATUS_PROCESSED);
        $prize->setAction($action);

        $this->em->persist($prize);
        $this->em->flush();

        return true;
    }

    /**
     * @param Prize $prize
     * @param string|null $address
     */
    private function processBankAction(Prize $prize, ?string $address)
    {
        $prize->setProcessStatus(Prize::PROCESS_STATUS_WAITING);
    }

    /**
     * @param Prize $prize
     * @param string|null $address
     */
    private function processConvertAction(Prize $prize, ?string $address)
    {
        $bonus = $this->bonusConverter->convert($prize->getAmount(), $this->params['prize.coefficient']);

        $user = $prize->getUser();
        $user->setBalance($user->getBalance() + $bonus);
        $this->em->persist($user);
    }

    /**
     * @param Prize $prize
     * @param string $address
     */
    private function processShippingAction(Prize $prize, string $address): void
    {
        $prize->setAddress($address);
        $prize->setProcessStatus(Prize::PROCESS_STATUS_WAITING);
    }

    /**
     * @param Prize $prize
     * @param string|null $address
     */
    private function processBonusAction(Prize $prize, ?string $address)
    {
        $user = $prize->getUser();
        $user->setBalance($user->getBalance() + $prize->getAmount());
        $this->em->persist($user);
    }
}