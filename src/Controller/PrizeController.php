<?php

namespace App\Controller;

use App\Service\PrizeService;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PrizeController extends AbstractAppController
{
    public function index(Request $request): Response
    {
        return $this->render('prize.html.twig', [
            'user' => $this->getUser(),
            'prize' => null,
            'shipping_form' => $this->getShippingForm()->createView(),
            'money_form' => $this->getMoneyForm()->createView()
        ]);
    }

    /**
     * @param PrizeService $prizeService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function prize(PrizeService $prizeService)
    {
        if (count($this->getUser()->getPrizes()) > 0) {
            return $this->redirect('/');
        }

        $prizeService->makePrize();

        return $this->redirect('/');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processPrize(Request $request, PrizeService $prizeService)
    {
        $formType = $request->get('form')['type'] ?? null;

        if (\is_null($formType)) {
            return $this->redirect('/');
        }

        $formType = 'get'.ucfirst($formType).'Form';
        if (!method_exists(self::class, $formType)) {
            return $this->redirect('/');
        }

        $form = $this->$formType();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $prizeService->processPrize($data['prize_id'], $data['action'], $data['address'] ?? null);
        }

        return $this->redirect('/');
    }

    /**
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getShippingForm()
    {
        return $this->createFormBuilder([])
            ->setAction($this->generateUrl('process_prize'))
            ->add('type', HiddenType::class)
            ->add('prize_id', HiddenType::class)
            ->add('action', HiddenType::class)
            ->add('address', TextareaType::class)
            ->getForm();
    }

    /**
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getMoneyForm()
    {
        return $this->createFormBuilder([])
            ->setAction($this->generateUrl('process_prize'))
            ->add('type', HiddenType::class)
            ->add('prize_id', HiddenType::class)
            ->add('action', HiddenType::class)
            ->getForm();
    }
}
