<?php

namespace App\Command;

use App\Repository\PrizeRepository;
use App\Service\BankService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SendMoneyCommand extends Command
{
    const BATCH_SIZE = 2;
    const BATCH_PAUSE = 1;  // Пауза между пачками в секундах

    protected static $defaultName = 'app:send_money';
    private PrizeRepository $prizeRepository;
    private EntityManagerInterface $em;
    private BankService $bankService;

    /**
     * SendMoneyCommand constructor.
     * @param string|null $name
     * @param PrizeRepository $prizeRepository
     * @param EntityManagerInterface $entityManager
     * @param BankService $bankService
     */
    public function __construct(string $name = null, PrizeRepository $prizeRepository, EntityManagerInterface $entityManager, BankService $bankService)
    {
        parent::__construct($name);
        $this->prizeRepository = $prizeRepository;
        $this->em = $entityManager;
        $this->bankService = $bankService;
    }

    protected function configure()
    {
        $this
            ->setDescription('Send money prize to bank account')
            ->addOption('size', null, InputOption::VALUE_OPTIONAL, 'Batch size')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $batchSize = ($input->getOption('size')) ? $input->getOption('size') : self::BATCH_SIZE;
        $pendingCount = $this->prizeRepository->getPendingBankReuestsCount();

        // Progress bar
        $progress = new ProgressBar($output, $pendingCount['pendingCount']);
        $progress->start();

        $i = 1;
        $pending = $this->prizeRepository->getPendingBankRequestsQuery();
        $iterableResult = $pending->toIterable();
        $items = [];

        foreach ($iterableResult as $item) {
            $items[] = $item;
            if (($i % $batchSize) === 0 || $pendingCount['pendingCount'] <= $i) {
                $this->bankService->processBatch($items);
                sleep(self::BATCH_PAUSE);
            }

            $progress->advance();
            ++$i;
        }

        $progress->finish();

        $io->success('Деньги отправлены!');

        return Command::SUCCESS;
    }
}
