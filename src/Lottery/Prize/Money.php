<?php

declare(strict_types=1);

namespace App\Lottery\Prize;

use App\Lottery\Prize;

class Money extends Prize implements MoneyInterface
{
    /**
     * @throws \Exception
     */
    public function generateRandomPrize(): void
    {
        $maxMoneyPrize = ($this->params['prize.balance'] >= $this->params['prize.money.max'])
            ? $this->params['prize.money.max']
            : $this->params['prize.balance'];

        $this->amount = random_int($this->params['prize.money.min'], $maxMoneyPrize);
    }

    /**
     * @return bool
     */
    public function checkAvailability(): bool
    {
        return $this->params['prize.balance'] >= $this->params['prize.money.min'];
    }
}