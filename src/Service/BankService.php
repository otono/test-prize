<?php

declare(strict_types=1);

namespace App\Service;

use App\Bank\Client;
use App\Entity\Prize;
use Doctrine\ORM\EntityManagerInterface;

class BankService
{
    private Client $bankClient;
    private EntityManagerInterface $entityManager;

    /**
     * BankService constructor.
     * @param Client $bankClient
     */
    public function __construct(Client $bankClient, EntityManagerInterface $entityManager)
    {
        $this->bankClient = $bankClient;
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $items
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function processBatch(array $items)
    {
        foreach ($items as $item) {
            $this->bankClient->sendMoney(
                $item->getUser()->getEmail(),
                $item->getAmount()
            );

            // Update status
            $item->setProcessStatus(Prize::PROCESS_STATUS_DONE);

            $this->entityManager->persist($item);
            $this->entityManager->flush();

            sleep(1);
        }
    }
}