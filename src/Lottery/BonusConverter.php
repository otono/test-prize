<?php

declare(strict_types=1);

namespace App\Lottery;

class BonusConverter
{
    /**
     * @param int $amount
     * @param float $coefficient
     * @return int
     */
    public function convert(int $amount, float $coefficient): int
    {
        return (int) round($amount * $coefficient);
    }
}