<?php

declare(strict_types=1);

namespace App\Lottery;

interface PrizeInterface
{
    /**
     * Generate prize
     * @return mixed
     */
    public function generateRandomPrize(): void;

    /**
     * Check prize availability
     * @return bool
     */
    public function checkAvailability(): bool;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return int
     */
    public function getAmount(): int;

    /**
     * @return array
     */
    public function getItem(): array;

    /**
     * @param array $params
     */
    public function setParams(array $params): void;

}