<?php

declare(strict_types=1);

namespace App\Lottery\Prize;

use App\Lottery\PrizeInterface;

interface ItemInterface extends PrizeInterface
{

}