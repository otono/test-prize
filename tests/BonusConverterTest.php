<?php

namespace App\Tests;

use App\Lottery\BonusConverter;
use PHPUnit\Framework\TestCase;

class BonusConverterTest extends TestCase
{
    public function testConvert()
    {
        $amount = (int) rand(5, 500);
        $coefficient = rand(1, 20) / 10;
        $assertResult = (int) round($amount * $coefficient);

        // Convert
        $converter = new BonusConverter();
        $result = $converter->convert($amount, $coefficient);

        $this->assertEquals($assertResult, $result);
    }
}
