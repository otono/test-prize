<?php

namespace App\Repository;

use App\Entity\Param;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Param|null find($id, $lockMode = null, $lockVersion = null)
 * @method Param|null findOneBy(array $criteria, array $orderBy = null)
 * @method Param[]    findAll()
 * @method Param[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Param::class);
    }

    /**
     * Get App money balance
     * @return int|mixed|string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getBalanceParam()
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.key = :key')
            ->setParameter('key', 'balance')
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get App money balance
     * @return int|mixed|string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCoefficientParam()
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.key = :key')
            ->setParameter('key', 'coefficient')
            ->getQuery()
            ->getOneOrNullResult();
    }
}
