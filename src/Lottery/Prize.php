<?php

declare(strict_types=1);

namespace App\Lottery;

abstract class Prize implements PrizeInterface
{
    protected array $params;
    protected string $type;
    protected int $amount = 1;
    protected array $item;

    /**
     * Prize constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->type = strtolower(substr(static::class,strrpos(static::class,'\\')+1));
        $this->params = $params;
    }

    /**
     * @param array $prizes
     * @param array $params
     * @return PrizeInterface
     * @throws \Exception
     */
    public static function createPrize(array $prizes, array $params): PrizeInterface
    {
        $prize = null;
        $available = false;

        while (!$available) {
            $class = $prizes[random_int(0, count($prizes)-1)];
            $prize = new $class($params);
            $available = $prize->checkAvailability();
        }

        return $prize;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getItem(): array
    {
        return $this->item;
    }
}